package com.primetherapeutics.demo.config;

import com.primetherapeutics.demo.domain.Guarantee;
import com.primetherapeutics.demo.services.client.GuaranteeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*") // UNRESTRICTED ACCESS, NOT FOR PRODUCTION
@RestController
@RequestMapping("/api")
public class GuaranteeController {

    private GuaranteeService guaranteeService;
    /*GuaranteeRepository guaranteeRepository;*/

    public GuaranteeController(GuaranteeService guaranteeService) { ///*, GuaranteeRepository guaranteeRepository*/) {
        this.guaranteeService = guaranteeService;
    }

    @GetMapping("/vendor/guarantee")
    public ResponseEntity<List<Guarantee>> getEdits() {
        List<Guarantee> list = guaranteeService.findAll();

        return ResponseEntity.status(HttpStatus.OK).body(list);
    }
}
