import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/invoicing/eligibility-edits',
      name: 'Eligibility Edits',
      component: () => import('./views/invoicing/EligibilityEdits.vue')
    },
    {
      path: '/otherscreen',
      name: 'otherscreen',
      component: () => import('./views/OtherScreen.vue')
    },
    {
      path: '/vendor/guarantee',
      name: 'Guarantee',
      component: () => import('./views/Guarantee.vue')
    }
  ]
})
